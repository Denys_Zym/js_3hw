// Перевірити, чи введене значення є цілим числом. Якщо ця умова не дотримується, повторювати виведення вікна на екран, доки не буде введено ціле число. в діапазоні від m до n (менше із введених чисел буде m, більше буде n). Якщо хоча б одне з чисел не відповідає умовам валідації, зазначеним вище, вивести повідомлення про помилку і запитати обидва числа знову.

let min = Number(prompt("Input number m (min)"));
while (!Number.isInteger(min) || !min || min === "string") {
  min = Number(prompt("Input number m (min)", min));
}

let max = Number(prompt("Input number n (max)"));
while (!Number.isInteger(max) || !max || max === "string") {
  max = Number(prompt("Input number n (max)", max));
}

function isPrime(m, n) {
  if (m > n) {
    console.log("Invalid condition! Try again");
    alert("Invalid condition! Try again");
    return;
  }
  nextPrime: for (let i = m; i <= n; i++) {
    for (let j = 2; j < i; j++) {
      if (i % j === 0) continue nextPrime;
    }
    console.log("i= ", i);
  }
}
console.log(isPrime(min, max));
